﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Util
{
    //常量类，用于汇总各种常量、参数
    public class ConstantHolderUtil
    {
        // 单例
        private static ConstantHolderUtil instance;

        //根目录
        public string CURRENT_DIRECTORY;
        //data目录
        public string DATA_DIRECTORY;
        //temp目录
        public string TEMP_DIRECTORY;
        //bin目录
        public string BIN_DIRECTORY;
        //test目录
        public string TEST_DIRECTORY;
        //dic目录
        public string DIC_DIRECTORY;
        //STM挖矿内核目录
        public string STM_DIRECTORY;
        // NHM挖矿内核目录
        public string NHM_DIRECTORY;
        //数据刷新时间,取数据间隔时间ms
        public int HEARTBEATTIME;

        //xmr-stak 内核运行参数，远程地址127.0.0.1:3335，
        public static string XMRSTAK_ARG =  "-c config_nh.txt -C pools_0-0.txt --noAMD --noNVIDIA ";
        public const string XMRSTAK_IP = "127.0.0.1";
        public const int XMRSTAK_PORT = 4002;

        //几种内核状态
        public enum CORE_STATE
        {
            [Description("闲")]
            CORE_IDLE,
            [Description("忙")]
            CORE_BUSY,
        }
        
        /// <summary>
        /// 保护构造方法
        /// </summary>
        protected ConstantHolderUtil()
        {
            DATA_DIRECTORY = System.IO.Directory.GetCurrentDirectory();
            HEARTBEATTIME = 2000;
        }

        /// <summary>
        /// 获得单例
        /// </summary>
        /// <returns></returns>
        public static ConstantHolderUtil GetInstance()
        {
            if (instance == null)
            {
                instance = new ConstantHolderUtil();
            }
            return instance;
        }

        


    }
}
