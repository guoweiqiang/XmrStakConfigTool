﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace XmrStakWin64
{
    /// <summary>
    /// 返回的json数据格式
    /// </summary>
    public class XmrStakWin64JsonModel
    {
        public string version { get; set; }

        public XmrStakWin64JsonHashrate hashrate { get; set; }
        public XmrStakWin64JsonResult results { get; set; }
    }

    public class XmrStakWin64JsonHashrate
    {
        //public float[] threads { get; set; }
        public double?[] total { get; set; }
        public float highest { get; set; }
    }
    public class XmrStakWin64JsonResult
    {
        public int diff_current { get; set; }
        public int shares_good { get; set; }
        public int shares_total { get; set; }
        public float avg_time { get; set; }
        public int hashes_total { get; set; }
    }

    public class cpu_threads_conf
    {
        public bool low_power_mode { get; set; }
        public bool no_prefetch { get; set; }
        public int affine_to_cpu { get; set; }
    }
}
