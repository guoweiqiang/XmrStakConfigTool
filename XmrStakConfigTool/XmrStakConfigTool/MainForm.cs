﻿using MyDAL.helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Util;
using XmrStakWin64;

namespace XmrStakConfigTool
{
    public partial class MainForm : Form
    {
        private double[] allResult = new double[50];
        private int stepCount = 0;
        private int step = 0;
        private int _best = -1;
        private string FileName = ConstantHolderUtil.GetInstance().DATA_DIRECTORY + "\\" + "xmr-stak\\mycpu_";
        public MainForm()
        {
            InitializeComponent();
            //richTextBox1.Text = "测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果测试时请关掉多余的软件，不要运行其他程序影响结果";
            label8.Text = "测试时请关掉多余的软件，不要运行其他程序影响结果\n" +
                "线程数越多的CPU测试项越多、速度越慢\n每一种配置方法测试2分钟，最后得出最高速度配置";
            initCpuInfo();
            initConfigFile();
            //progressBar1.Maximum = 
            Thread t1 = new Thread(new ThreadStart(heartbeatMethod));
            t1.IsBackground = true;
            t1.Start();
        }
        private void initConfigFileTwoCpu()
        {
            int processorsCount = CpuHelper.GetInstance().ProcessorsCount[0] + CpuHelper.GetInstance().ProcessorsCount[1];
            int[] affine = new int[processorsCount];
            int tep = 0;
            int jname = 0;
            int i = 0;
            for (i = 0; tep < processorsCount; i++)
            {
                affine[i] = tep;//所有偶数
                tep += 2;
            }
            CpuHelper.GetInstance().WriteJsonFile(new XmrStakConfigCpu(affine, i, false), FileName + jname + ".txt");
            jname++;
            CpuHelper.GetInstance().WriteJsonFile(new XmrStakConfigCpu(affine, i, true), FileName + jname + ".txt");
            jname++;
            //开始加奇数
            tep = 1;

            for (; i < processorsCount; i++)//开始加奇数
            {
                affine[i] = tep;
                tep += 2;
                CpuHelper.GetInstance().WriteJsonFile(new XmrStakConfigCpu(affine, i + 1, false), FileName + jname + ".txt");
                jname++;
                CpuHelper.GetInstance().WriteJsonFile(new XmrStakConfigCpu(affine, i + 1, true), FileName + jname + ".txt");
                jname++;
            }
            stepCount = jname;
            progressBar1.Maximum = stepCount;
            label2.Text = "测试进度：(" + step + " / " + stepCount + ")";

            //for (i ; i < stepCount; i++)
            //{
            //    cpu_threads_conf n = new cpu_threads_conf();
            //    n.affine_to_cpu = false;
            //    XmlHelper.ObjectToXMLFile(JobTechnologicalProcesslListHeader, FileName+ i, Encoding.Unicode);
            //}
        }
        private void initConfigFile()
        {
            int[] affine = new int[CpuHelper.GetInstance().ProcessorsCount[0]];
            int tep = 0;
            int jname = 0;
            int i = 0;
            for (i = 0; tep < CpuHelper.GetInstance().ProcessorsCount[0]; i++)
            {
                affine[i] = tep;//所有偶数
                tep += 2;
            }
            CpuHelper.GetInstance().WriteJsonFile(new XmrStakConfigCpu(affine,i, false), FileName + jname + ".txt");
            jname++;
            CpuHelper.GetInstance().WriteJsonFile(new XmrStakConfigCpu(affine, i, true), FileName + jname + ".txt");
            jname++;
            //开始加奇数
            tep = 1;
            
            for (; i < CpuHelper.GetInstance().ProcessorsCount[0]; i++)//开始加奇数
            {
                affine[i] = tep;
                tep += 2;
                CpuHelper.GetInstance().WriteJsonFile(new XmrStakConfigCpu(affine, i+1,false), FileName + jname +".txt");
                jname++;
                CpuHelper.GetInstance().WriteJsonFile(new XmrStakConfigCpu(affine, i + 1, true), FileName + jname + ".txt");
                jname++;
            }
            stepCount = jname;
            progressBar1.Maximum = stepCount;
            label2.Text = "测试进度：(" + step + " / " + stepCount + ")";

            //for (i ; i < stepCount; i++)
            //{
            //    cpu_threads_conf n = new cpu_threads_conf();
            //    n.affine_to_cpu = false;
            //    XmlHelper.ObjectToXMLFile(JobTechnologicalProcesslListHeader, FileName+ i, Encoding.Unicode);
            //}
        }

        private void initCpuInfo()
        {
            label3.Text = "CPU个数 = " + CpuHelper.GetInstance().cpuCount;
            if (CpuHelper.GetInstance().cpuCount == 1)
            {
                label4.Text = "CPU名字 = " + CpuHelper.GetInstance().cpuName[0];
                label5.Text = "CPU核心数 = " + CpuHelper.GetInstance().coreCount[0];
                label6.Text = "CPU线程数 = " + CpuHelper.GetInstance().ProcessorsCount[0];
                //stepCount = CpuHelper.GetInstance().ProcessorsCount[0] - CpuHelper.GetInstance().coreCount[0] + 5 + 2;
                //progressBar1.Maximum = stepCount;
                //label2.Text = "测试进度：（" + stepCount + " \\ "+ step + "）";
            }
            else
            {
                for (int i = 0; i < CpuHelper.GetInstance().cpuCount; i++)
                {
                    label4.Text = "CPU名字 = " + CpuHelper.GetInstance().cpuName[0];
                    label5.Text = "CPU总核心数 = " + CpuHelper.GetInstance().coreCount[0] *CpuHelper.GetInstance().cpuCount;
                    label6.Text = "CPU总线程数 = " + CpuHelper.GetInstance().ProcessorsCount[0] ;
                }
               
                //stepCount = CpuHelper.GetInstance().ProcessorsCount[0] - CpuHelper.GetInstance().coreCount[0] + 5 + 2+ CpuHelper.GetInstance().ProcessorsCount[1] - CpuHelper.GetInstance().coreCount[1] + 5 + 2;
            }
        }

        public void heartbeatMethod()
        {
            Helpers.ConsolePrint("MainForm", "start new thread for show miner info");
            
            while (true)
            {
                Thread.Sleep(ConstantHolderUtil.GetInstance().HEARTBEATTIME);
                if (stepCount == step)
                {
                    this.Invoke(new Action(() =>
                    {
                        double tep = 0;
                        button3.Enabled = true;
                        label2.Text = "测试进度：(完成)";
                        progressBar1.Value = step;
                        for (int i = 0; i < step; i++)
                        {
                            if (allResult[i] > tep)
                            {
                                tep = allResult[i];
                                _best = i;
                                label7.Text = "本次最优：" + tep;
                            }
                        }
                        button2.Enabled = true;
                        button1.Enabled = false;
                    }));
                    continue;
                }
                if (XmrStakWin64Helper.GetInstance().isConnect && XmrStakWin64Helper.GetInstance().totalSpeed != 0)
                {
                    allResult[step] = XmrStakWin64Helper.GetInstance().totalSpeed;
                    this.Invoke(new Action(() =>
                    {
                        richTextBox1.Text = "";
                        for (int i = 0; i <= step; i++)
                        {

                            richTextBox1.Text += "  第" + i + "：" + allResult[i];
                        }
                        progressBar1.Value = step;
                        label2.Text = "测试进度：(" + step  + " / " + stepCount + ")";

                    }));
                }
                
               
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            XmrStakWin64Helper.GetInstance().Stop();
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            step=0;
            label7.Text = "等待结果";
            label2.Text = "测试进度：(" + step  + " / " + stepCount + ")";
            progressBar1.Value = step;
            richTextBox1.Text = "";
            Thread t1 = new Thread(new ThreadStart(StartMethod));
            t1.IsBackground = true;
            t1.Start();
            button2.Enabled = false;
            button1.Enabled = true;
            _best = -1;
            button3.Enabled = false;
        }

        private void StartMethod()
        {
            string cpustr = "";
            for (int i = 0; i < stepCount; i++)
            {
                cpustr = "--cpu mycpu_" + step + ".txt";
                step = i;
                XmrStakWin64Helper.GetInstance().Start(ConstantHolderUtil.XMRSTAK_ARG + cpustr);
                Thread.Sleep(1000*60*2);
                XmrStakWin64Helper.GetInstance().Stop();
            }
            step = stepCount;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            XmrStakWin64Helper.GetInstance().Stop();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (_best != -1)
            {
                System.Diagnostics.Process.Start("notepad.exe", FileName + _best + ".txt");
            }
        }
    }
}
